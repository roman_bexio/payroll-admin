package com.bexio.payroll.admin.test

import com.bexio.payroll.admin.infrastructure.CorrelationId
import org.scalatest.{FlatSpec, Matchers}

trait BaseTest extends FlatSpec with Matchers {
  CorrelationId.init()
  val testClock = new TestClock()
}
