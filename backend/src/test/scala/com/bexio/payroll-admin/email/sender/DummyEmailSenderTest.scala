package com.bexio.payroll.admin.email.sender

import com.bexio.payroll.admin.email.EmailData
import com.bexio.payroll.admin.test.BaseTest
import monix.execution.Scheduler.Implicits.global

class DummyEmailSenderTest extends BaseTest {
  it should "send scheduled email" in {
    DummyEmailSender(EmailData("test@sml.com", "subject", "content")).runSyncUnsafe()
    DummyEmailSender.findSentEmail("test@sml.com", "subject") shouldBe 'defined
  }
}
