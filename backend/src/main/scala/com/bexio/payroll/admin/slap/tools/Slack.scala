package com.bexio.payroll.admin.slap.tools

import com.typesafe.scalalogging._

import io.circe._
import io.circe.generic.auto._
import io.circe.syntax._

import scala.collection.immutable

import com.bexio.payroll.admin.slap.Config

object Slack extends LazyLogging {

  final case class Attachment(
      title: Option[String] = None,
      text: String,
      fallback: Option[String] = None,
      image_url: Option[String] = None,
      thumb_url: Option[String] = None,
      title_link: Option[String] = None,
      color: Option[String] = None,
      pretext: Option[String] = None,
      author_name: Option[String] = None,
      author_link: Option[String] = None,
      author_icon: Option[String] = None,
      mrkdwn_in: Option[List[String]] = Some(List("text", "pretext"))
  )

  final case class Payload(
      text: Option[String] = None,
      channel: Option[String] = None,
      username: Option[String] = None,
      icon_url: Option[String] = None,
      icon_emoji: Option[String] = None,
      attachments: Option[Seq[Attachment]] = None
  )

  def send(msg: String, url: String): Unit = {
    val printer = Printer.noSpaces.copy(dropNullValues = true)

    val message = printer
      .pretty(
        Payload(
          username = Some(Config.botName),
          icon_url = Some(Config.botIcon),
          attachments = Some(
            immutable.Seq(
              Attachment(
                text = msg,
                color = Some(Config.botColor),
                pretext = Some(url),
                title = Some(s"API status on ${extractDomainName(url)}")
              )
            )
          )
        ).asJson
      )

    logger.warn(s"Slack => $message")

    WebClient
      .postJson(Config.slackUrl, message)
      .map { status =>
        {
          if (!status.isSuccess)
            logger.warn(s"POST ${Config.slackUrl} responded with $status status.")
        }
      }
  }
}
