package com.bexio.payroll.admin.slap.tools.CloudStorage

import java.io.ByteArrayInputStream

import awscala.Region
import awscala.s3.{ Bucket, PutObjectResult, S3 }
import cats.effect._
import cats.implicits._
import com.amazonaws.services.s3.model.ObjectMetadata
import com.typesafe.scalalogging.LazyLogging

import com.bexio.payroll.admin.slap.{ Config, SlapResult }
import com.bexio.payroll.admin.slap.tools.{ WithCharsetEncoder, runSafely }

object Aws extends CloudProvider with LazyLogging with WithCharsetEncoder {

  implicit val region: Region = Region(Config.s3Region)
  implicit val s3: S3 = S3(accessKeyId = Config.s3AccessId, secretAccessKey = Config.s3AccessSecret)

  lazy val bucket: Bucket = s3.bucket(Config.s3Name).getOrElse(s3.createBucket(Config.s3Name))

  def put(
      name: String,
      content: String
  ): SlapResult[PutObjectResult] =
    runSafely(logErrorPrefix = s"Could not transfer file $content to AWS S3")(IO {
      val stream = new ByteArrayInputStream(content.getBytes(charset))
      bucket.putObject(name, stream, new ObjectMetadata)
    })

  def get(file: String): SlapResult[String] =
    runSafely(logErrorPrefix = s"Could not read file $file from AWS S3")(
      IO(
        bucket
          .get(file)
          .map(s3Obj => scala.io.Source.fromInputStream(s3Obj.content).mkString)
      )
    ).flatMap {
      case Some(context) => Right(context)
      case None          => Left(s"Got an empty object from AWS S3 $file")
    }
}
