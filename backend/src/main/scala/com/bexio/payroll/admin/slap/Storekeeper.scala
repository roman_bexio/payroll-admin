package com.bexio.payroll.admin.slap

import akka.actor.{ Actor, ActorLogging, Props }
import com.typesafe.scalalogging.LazyLogging

object Storekeeper {

  final case class Persist(content: String, localFileName: String, cloudFileName: String)

  def props: Props = Props[Storekeeper]
}

class Storekeeper extends Actor with ActorLogging with LazyLogging {
  import tools._
  import Storekeeper._

  def receive: PartialFunction[Any, Unit] = {

    case Persist(content, localFileName, cloudFileName) =>
      FileStorage.save(
        file = localFileName,
        content = content
      )
      CloudStorage.Google.put(
        name = cloudFileName,
        content = content
      )
  }
}
