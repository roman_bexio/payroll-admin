package com.bexio.payroll.admin.slap

import java.text.SimpleDateFormat
import com.typesafe.scalalogging.LazyLogging
import cats.effect._
import cats.implicits._

package object tools extends LazyLogging {

  def runSafely[T](logErrorPrefix: SlapError)(fun: IO[T]): SlapResult[T] =
    fun.attempt
      .unsafeRunSync()
      .leftFlatMap(e => {
        logger.error(s"$logErrorPrefix : $e")
        Left(e.toString)
      })

  def extractDomainName(url: String): String = {
    val pattern = """^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)""".r

    pattern
      .findAllIn(url)
      .matchData
      .map(_.group(1))
      .toSeq
      .headOption
      .getOrElse("")
  }

  def timestamp: String =
    new SimpleDateFormat("yyyyMMdd'_'HH-mm")
      .format(System.currentTimeMillis())

}
