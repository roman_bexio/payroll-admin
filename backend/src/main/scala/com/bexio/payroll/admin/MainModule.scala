package com.bexio.payroll.admin

import java.time.Clock

import cats.data.NonEmptyList
import com.bexio.payroll.admin.email.EmailModule
import com.bexio.payroll.admin.http.{Http, HttpApi}
import com.bexio.payroll.admin.infrastructure.InfrastructureModule
import com.bexio.payroll.admin.metrics.MetricsModule
import com.bexio.payroll.admin.passwordreset.PasswordResetModule
import com.bexio.payroll.admin.security.SecurityModule
import com.bexio.payroll.admin.user.UserModule
import com.bexio.payroll.admin.util.{DefaultIdGenerator, IdGenerator, ServerEndpoints}
import monix.eval.Task

/**
  * Main application module. Depends on resources initalised in [[InitModule]].
  */
trait MainModule
    extends SecurityModule
    with EmailModule
    with UserModule
    with PasswordResetModule
    with MetricsModule
    with InfrastructureModule {

  override lazy val idGenerator: IdGenerator = DefaultIdGenerator
  override lazy val clock: Clock = Clock.systemUTC()

  lazy val http: Http = new Http()

  private lazy val endpoints: ServerEndpoints = userApi.endpoints concatNel passwordResetApi.endpoints
  private lazy val adminEndpoints: ServerEndpoints = NonEmptyList.of(metricsApi.metricsEndpoint, versionApi.versionEndpoint)

  lazy val httpApi: HttpApi = new HttpApi(http, endpoints, adminEndpoints, collectorRegistry, config.api)

  lazy val startBackgroundProcesses: Task[Unit] = emailService.startProcesses().void
}
