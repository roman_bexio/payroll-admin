package com.bexio.payroll.admin.slap

import akka.actor.{ Actor, ActorLogging, Props }
import com.typesafe.scalalogging.LazyLogging

object Informer {

  final case class Pulse(msg: String)
  final case class Slack(msg: String, url: String)
  final case class DBLog(status: String, url: String)

  def props: Props = Props[Informer]
}

class Informer extends Actor with ActorLogging with LazyLogging {
  import Informer._

  def receive: PartialFunction[Any, Unit] = {

    case Pulse(msg) =>
      logger.debug(msg)

    case Slack(report, url) =>
      tools.Slack.send(report, url)

    case DBLog(report, url) =>
      tools.DataBase.updateUrlStatus(report, url)
  }
}
