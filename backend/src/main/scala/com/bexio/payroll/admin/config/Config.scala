package com.bexio.payroll.admin.config

import com.bexio.payroll.admin.email.EmailConfig
import com.bexio.payroll.admin.http.HttpConfig
import com.bexio.payroll.admin.infrastructure.DBConfig
import com.bexio.payroll.admin.passwordreset.PasswordResetConfig
import com.bexio.payroll.admin.user.UserConfig

/**
  * Maps to the `application.conf` file. Configuration for all modules of the application.
  */
case class Config(db: DBConfig, api: HttpConfig, email: EmailConfig, passwordReset: PasswordResetConfig, user: UserConfig)
