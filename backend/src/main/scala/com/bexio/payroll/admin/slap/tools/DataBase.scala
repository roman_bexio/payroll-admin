package com.bexio.payroll.admin.slap.tools

import com.typesafe.scalalogging.LazyLogging

object DataBase extends LazyLogging {
  def updateUrlStatus(url: String, report: String): Unit = {}

  def insertUrlSlap(url: String, report: String): Unit = {}

  def deleteUrlSlap(url: String): Unit = {}

  def readUrlSlap(url: String): Unit = {}

  def readAllUrlSlap = Map.empty[String, (String, Option[String])]
}
