package com.bexio.payroll.admin.user

import scala.concurrent.duration.Duration

case class UserConfig(defaultApiKeyValid: Duration)
