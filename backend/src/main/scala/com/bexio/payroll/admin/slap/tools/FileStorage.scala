package com.bexio.payroll.admin.slap.tools

import java.io.FileOutputStream
import java.nio.file.{ Files, Paths }
import java.nio.charset.{ CharsetDecoder, CodingErrorAction }

import scala.io.Source
import cats.effect._
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import com.bexio.payroll.admin.slap.{ Config, SlapResult }

object FileStorage extends LazyLogging with WithCharsetEncoder {

  import scala.util.Try

  val decoder: CharsetDecoder = codec.decoder.onMalformedInput(CodingErrorAction.IGNORE)

  def read(file: String): SlapResult[String] =
    runSafely(logErrorPrefix = "IO error during reading the stored file")(
      Resource
        .fromAutoCloseable(IO(Source.fromFile(file)(decoder)))
        .use(source => IO(source.getLines.mkString))
    )

  def save(file: String, content: String): SlapResult[String] =
    runSafely(logErrorPrefix = "IO error during saving the file")(
      Resource
        .fromAutoCloseable(IO(new FileOutputStream(new java.io.File(file))))
        .use(source => IO(source.write(content.getBytes(codec.charSet))))
        .map(_ => content)
    )

  def exists(file: String): Boolean = Try(Files.exists(Paths.get(file))).toOption.getOrElse(false)

}
