package com.bexio.payroll.admin.slap

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem, Scheduler }
import akka.util.Timeout
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.scalalogging._
import scala.concurrent.duration._
import scala.concurrent.{ Await, ExecutionContext, ExecutionContextExecutor }

object SlapSwaggerApp extends LazyLogging {
  import tools._
  import Overseer._

  val system = ActorSystem("SlapSwaggerSystem")

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit def executor: ExecutionContext = system.dispatcher
  implicit def scheduler: Scheduler = system.scheduler
  implicit val timeout: Timeout = Timeout(300, TimeUnit.SECONDS)

  val jobScheduler: QuartzSchedulerExtension = QuartzSchedulerExtension(system)

  val overseer: ActorRef = system.actorOf(Overseer.props, name = "overseerActor")

    // TODO : fetch from DB
    DataBase
      .readAllUrlSlap
//  val urls =
    //    Config.url.split(", ")
    //    // TODO : unique file name per tracking task
    //    // currently we support only one track per domain
    //    .groupBy(extractDomainName)
    //    .mapValues(_.head)
    //    .values

      .foreach { case(url, (quartz, calendar)) =>
          logger.info(
            s"Starting to keep track on api-doc at $url scheduled with $quartz / $calendar.\n"
          )

    try {
      jobScheduler.createJobSchedule(
        name = "slap_" + extractDomainName(url),
        receiver = overseer,
        msg = Slap(url),
        cronExpression = quartz,
        calendar = calendar
      )
    } catch {
      case iae: IllegalArgumentException =>
        logger.error(
          s"Failed scheduling of $url with Quartz expression $quartz / $calendar.",
          iae
        )
    }

    // first run
    overseer ! Slap(url)
  }
}
