package com.bexio.payroll.admin.http

case class HttpConfig(host: String, port: Int)
