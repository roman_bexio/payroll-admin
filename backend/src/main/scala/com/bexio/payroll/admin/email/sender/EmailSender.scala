package com.bexio.payroll.admin.email.sender

import com.bexio.payroll.admin.email.EmailData
import monix.eval.Task

trait EmailSender {
  def apply(email: EmailData): Task[Unit]
}
