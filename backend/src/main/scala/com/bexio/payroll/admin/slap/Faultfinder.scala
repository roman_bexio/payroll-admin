package com.bexio.payroll.admin.slap

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import com.typesafe.scalalogging._

object Faultfinder extends LazyLogging {

  def props: Props = Props[Faultfinder]
}

class Faultfinder extends Actor with ActorLogging with LazyLogging {
  import tools._
  import Overseer._
  import Storekeeper._

  val storage: ActorRef = context.system.actorOf(Storekeeper.props, name = "storageActor")

  def receive: PartialFunction[Any, Unit] = {

    case Slap(url) =>
      sender ! Scream(slap(url), url)
  }

  def slap(url: String): Either[String, String] = {

    lazy val tmpFileName: String = "%s/%s.json"
      .format(Config.workingDir, extractDomainName(url))
    lazy val cloudSnapshotFileName: String = "%s.json"
      .format(extractDomainName(url))

    for {
      actual <- WebClient.get(url) // breaks if api-doc is unavailable
      // if absent locally, then fetch from the cloud storage first
      stored <- {
        if (FileStorage.exists(file = tmpFileName)) {
          FileStorage.read(file = tmpFileName)
        } else {
          CloudStorage.Google
            .get(file = cloudSnapshotFileName)
            .fold(
              // the case when performs the first check of the API
              _ => {
                storage ! Persist(
                  content = actual,
                  localFileName = tmpFileName,
                  cloudFileName = cloudSnapshotFileName
                )
                Right(actual)
              },
              // save locally and perform if nothing was changed since then
              content =>
                FileStorage.save(
                  file = tmpFileName,
                  content = content
              )
            )
        }
      }
      // compare new api-doc with the preserved one
      diff <- JsonComparator.diff(actual, stored)
      res <- {
        val report = JsonComparator.prettyReport(diff.ops)
        if (report.nonEmpty) {
          // in case of detected changes
          // save the raw diff
          val rawDiffFileName = s"${extractDomainName(url)}_$timestamp.json"
          storage ! Persist(
            content = diff.toString,
            localFileName = s"${Config.workingDir}/$rawDiffFileName",
            cloudFileName = rawDiffFileName
          )
          // save the snapshot
          storage ! Persist(
            content = actual,
            localFileName = tmpFileName,
            cloudFileName = cloudSnapshotFileName
          )
          Left(report)
        } else {
          // otherwise we are all happy
          Right(s"No changes so far on $url")
        }
      }
    } yield res
  }
}
