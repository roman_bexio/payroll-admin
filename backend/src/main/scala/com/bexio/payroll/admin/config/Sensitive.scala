package com.bexio.payroll.admin.config

case class Sensitive(value: String) extends AnyVal {
  override def toString: String = "***"
}
