package com.bexio.payroll.admin.user

import com.bexio.payroll.admin.email.{EmailScheduler, EmailTemplates}
import com.bexio.payroll.admin.http.Http
import com.bexio.payroll.admin.security.{ApiKey, ApiKeyService, Auth}
import com.bexio.payroll.admin.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait UserModule extends BaseModule {
  lazy val userModel = new UserModel
  lazy val userApi = new UserApi(http, apiKeyAuth, userService, xa)
  lazy val userService = new UserService(userModel, emailScheduler, emailTemplates, apiKeyService, idGenerator, clock, config.user)

  def http: Http
  def apiKeyAuth: Auth[ApiKey]
  def emailScheduler: EmailScheduler
  def emailTemplates: EmailTemplates
  def apiKeyService: ApiKeyService
  def xa: Transactor[Task]
}
