package com.bexio.payroll.admin.slap.tools.CloudStorage

import cats.effect._
import cats.implicits._

import com.google.auth.oauth2.GoogleCredentials
import com.google.common.collect.Lists
import com.google.cloud.storage._

import com.bexio.payroll.admin.slap.{ Config, SlapResult }
import com.bexio.payroll.admin.slap.tools.{ WithCharsetEncoder, FileStorage, runSafely }

import scala.util.Try


object Google extends CloudProvider with WithCharsetEncoder {

  lazy val storage: Storage =
    {
      // check if GOOGLE_APPLICATION_CREDENTIALS exists
      if (FileStorage.exists(Config.gcsCreds)) {
        Some(defaultStorage)
      } else {
        None
      }
    }.orElse {
      Try(
        {
          val credentials = GoogleCredentials
            .fromStream(new java.io.ByteArrayInputStream(Config.gcsCreds.getBytes(java.nio.charset.StandardCharsets.UTF_8.name)))
            .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
          StorageOptions
            .newBuilder()
            .setCredentials(credentials)
            .build().getService
        }
      ).toOption
    }
      .getOrElse(defaultStorage)


  def put(
    name: String,
    content: String
  ): SlapResult[_] =
    runSafely(logErrorPrefix = s"Could not transfer content of $name to Cloud Storage")(IO {
      val blobId = BlobId.of(Config.gcs3Name, name)
      val blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/json").build
      storage.create(blobInfo, content.getBytes(charset))
    })

  def get(file: String): SlapResult[String] =
    runSafely(logErrorPrefix = s"Could not read file $file from Cloud Storage") {
      IO(
        storage.get(BlobId.of(Config.gcs3Name, file)).getContent().map(_.toChar).mkString
      )
    }

  private def defaultStorage = StorageOptions.getDefaultInstance.getService

}
