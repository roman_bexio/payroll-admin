package com.bexio.payroll.admin.util

import java.time.Clock

import com.bexio.payroll.admin.config.Config

trait BaseModule {
  def idGenerator: IdGenerator
  def clock: Clock
  def config: Config
}
