package com.bexio.payroll.admin.slap.tools

import java.nio.charset.Charset
import com.bexio.payroll.admin.slap.Config
import scala.io.Codec

trait WithCharsetEncoder {
  protected val charset: Charset = Charset.forName(Config.charset)
  implicit val codec: Codec = new Codec(charset)
}
