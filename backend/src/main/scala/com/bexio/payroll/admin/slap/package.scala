package com.bexio.payroll.admin

package object slap {

  type SlapError = String
  type SlapResult[T] = Either[SlapError, T]
}
