package com.bexio.payroll.admin.slap.tools

import gnieh.diffson._
import gnieh.diffson.circe._
import io.circe._
import cats.effect._
import cats.implicits._

import com.bexio.payroll.admin.slap.SlapResult

object JsonComparator {

  implicit val lcs: Patience[Json] = new Patience[Json]

  type DiffOpsFilter = Operation => Boolean

  // TODO: move to config
  lazy val PathToIgnoreRegex = List(
//    """.*/description""".r,
    """.*/example""".r
  )

  def diff(after: String, before: String): SlapResult[circe.JsonPatch] =
    runSafely(logErrorPrefix = "Comparision error")(
      IO(JsonDiff.diff(before, after, remember = true))
    )

  // get human-readable path from JsonPointer
  private lazy val pretty: JsonPointer => String =
    p =>
      Pointer
        .unapplySeq(p.path)
        .map(_.foldLeft("")(_ + "/" + _.fold(identity, _.toString)))
        .getOrElse("...")

  private lazy val ignoreWhitespaces: DiffOpsFilter = {
    case Replace(_, value, old) =>
      !old
        .map(_.noSpaces.replaceAll("\\s+", " "))
        .contains(value.noSpaces.replaceAll("\\s+", " "))
    case _ => true
  }

  private lazy val showIfShort: Option[String] => String =
    _.map(value => {
      if (value.length < 80) s"\t\t_${value}_\n"
      else ""
    }).getOrElse("")

  private val excludeByPath: DiffOpsFilter =
    op => PathToIgnoreRegex.forall(_.findFirstMatchIn(pretty(op.path)).isEmpty)

  lazy val prettyReport: List[Operation] => String =
    _.filter(ignoreWhitespaces)
      .filter(excludeByPath)
      .groupBy(_.getClass)
      .mapValues(changes => {
        changes
          .sortBy(p => pretty(p.path))
          .foldLeft {
            // Title
            changes.headOption.fold(ifEmpty = "") {
              case _: Add     => "*ADDED*:\n"
              case _: Remove  => "*REMOVED*:\n"
              case _: Replace => "*CHANGED*:\n"
              case _: Move    => "*MOVED*:\n"
              case _: Copy    => "*COPIED::\n"
              case _          => "...\n"
            }
          } {
            // items
            case (acc, elem) =>
              elem match {
                case Add(path, value) =>
                  s"$acc\t${pretty(path)}\n" + showIfShort(Some(value.noSpaces))
                case Remove(path, _) =>
                  s"$acc\t~${pretty(path)}~\n"
                case Replace(path, value, maybeOld) =>
                  s"$acc\t${pretty(path)}:\n\t\t_${value.noSpaces}_\n" + showIfShort(
                    maybeOld.map(_.noSpaces)
                  )
                case Move(from, path) =>
                  s"$acc\t~${pretty(from)}~ to _${pretty(path)}_\n"
                case Copy(from, path) =>
                  s"$acc\t_${pretty(from)}_ to _${pretty(path)}_\n"
                case other: Operation =>
                  s"$acc\t$other\n"
              }
          }
      })
      .values
      .toSeq
      .sorted
      .mkString("\n")
}
