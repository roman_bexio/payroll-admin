package com.bexio.payroll.admin.slap.tools

import cats.effect._
import cats.implicits._
import sttp.client._
import sttp.model._

import com.bexio.payroll.admin.slap.SlapResult

object WebClient {

//  implicit val backend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()
  implicit val backend = HttpURLConnectionBackend()

  def get(url: String, expectedStatus: Int = 200): SlapResult[String] =
    runSafely(logErrorPrefix = s"IO error during fetching from the URL")(
      IO {
        val response = 
          basicRequest
            .get(uri"${url.trim}")
            .send()

        if (response.code == expectedStatus) {
          response.body
        } else {
          Left(s"Expected $expectedStatus but responded with ${response.code} status.")
        }
    }).fold(Left(_), identity(_))

  def postJson(url: String, message: String): SlapResult[StatusCode] =
    runSafely(logErrorPrefix = "Failed to send Slack message")(
      IO(
        basicRequest
          .header("Content-type", "application/json")
          .body(message)
          .post(uri"$url")
          .send()
          .code
      )
    )
}
