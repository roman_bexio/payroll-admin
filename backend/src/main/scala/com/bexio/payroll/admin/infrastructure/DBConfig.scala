package com.bexio.payroll.admin.infrastructure

import com.bexio.payroll.admin.config.Sensitive

case class DBConfig(username: String, password: Sensitive, url: String, migrateOnStart: Boolean, driver: String, connectThreadPoolSize: Int)
