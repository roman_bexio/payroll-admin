package com.bexio.payroll.admin.slap.tools.CloudStorage

import com.bexio.payroll.admin.slap.SlapResult

trait CloudProvider {
  def put(name: String, content: String): SlapResult[_]

  def get(file: String): SlapResult[_]
}
