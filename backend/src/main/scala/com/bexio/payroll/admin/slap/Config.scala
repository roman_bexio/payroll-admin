package com.bexio.payroll.admin.slap

import com.typesafe.config._

object Config {

  val conf: Config = ConfigFactory.load()

  lazy val url: String = conf.getString("url")

  lazy val scheduleCronExpression: String = conf.getString("scheduleCronExpression")

  lazy val scheduleCalendar: Option[String] = {
    if (conf.hasPathOrNull("scheduleCalendar")) Option(conf.getString("scheduleCalendar"))
    else None
  }

  lazy val slackUrl: String = conf.getString("slackUrl")

  lazy val botName: String = conf.getString("bot_name")

  lazy val botIcon: String = conf.getString("bot_icon")

  lazy val botColor: String = conf.getString("bot_color")

  lazy val workingDir: String = conf.getString("working_dir")

  lazy val charset: String = conf.getString("charset")

  // GCS
  lazy val gcs3Name: String = conf.getString("gcs.name")

  lazy val gcsCreds: String = conf.getString("gcs.creds")

  // AWS S3
  lazy val s3Name: String = conf.getString("s3.name")

  lazy val s3Region: String = conf.getString("s3.region")

  lazy val s3AccessId: String = conf.getString("s3.accessKeyId")

  lazy val s3AccessSecret: String = conf.getString("s3.secretAccessKey")

  lazy val s3WorkingDir: String = conf.getString("s3.folder")
}
