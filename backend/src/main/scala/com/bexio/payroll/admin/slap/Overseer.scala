package com.bexio.payroll.admin.slap

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import com.typesafe.scalalogging.LazyLogging

object Overseer {

  final case class Slap(url: String)
  final case class Scream(tears: Either[String, String], url: String)

  def props: Props = Props[Overseer]
}

class Overseer extends Actor with ActorLogging with LazyLogging {
  import Informer._
  import Overseer._

  val informer: ActorRef = context.system.actorOf(Informer.props, name = "reporterActor")
  val faultfinder: ActorRef = context.system.actorOf(Faultfinder.props, name = "faultfinderActor")

  def receive: PartialFunction[Any, Unit] = {

    case Slap(url) =>
      faultfinder ! Slap(url)

    case Scream(tears, url) =>
      tears
      // forward Left => Slack/DB, Right => logger.debug
        .fold(
          // push notification in case of non empty diff or broken flow due to an error
          report => {
            // we don't send empty reports
            if (report.nonEmpty) {
              informer ! Slack(msg = report, url = url)
              informer ! DBLog(status = report, url = url)
            }
            else {
              informer ! Pulse(s"No important changes were detected on $url")
            }
          },
          // pulse output if happy
          pulse => informer ! Pulse(pulse)
        )
  }
}
