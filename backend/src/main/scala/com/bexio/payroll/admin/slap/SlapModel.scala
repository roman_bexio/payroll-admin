package com.bexio.payroll.admin.slap

import java.time.Instant

import cats.implicits._
import cats.data.NonEmptyList
import com.bexio.payroll.admin.infrastructure.Doobie._


class SlapModel {

  def findAll: doobie.ConnectionIO[NonEmptyList[Slap]] = {
    (sql"SELECT url, schedule, only_busyness_hours, status, last_update FROM slaps")
      .query[Slap]
      .nel
  }

  def findByUrl(url: String): ConnectionIO[Option[Slap]] = {
    findBy(fr"url = $url")
  }

  private def findBy(by: Fragment): ConnectionIO[Option[Slap]] = {
    (sql"SELECT url, schedule, only_busyness_hours, status, last_update FROM slaps WHERE " ++ by)
      .query[Slap]
      .option
  }
}

case class Slap(
  url: String,
  schedule: String,
  only_busyness_hours: Boolean,
  status: String,
  last_update: Instant
)
